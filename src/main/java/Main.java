import java.io.IOException;

public class Main
{
    public static void main(String[] args) throws IOException {
        ExecuteCommand_helper e = new ExecuteCommand_helper();
        e.makeConnection();
        e.execute();
    }
}
